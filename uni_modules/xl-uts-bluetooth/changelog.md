## 1.0.11（2024-07-27）
- Android端notifyBLECharacteristicValueChange接口优化
- BLE.js添加异步api的Promise风格调用的兼容性代码
- 屏蔽iOS端某些调试日志的打印
- readme更新，添加Promise风格相关的文档
## 1.0.10（2024-07-23）
- 修复IOS平台的BLE特征的write字段和Android平台不统一的问题
- 修复误报10007的问题
## 1.0.9（2024-07-22）
- 修复IOS编译失败的问题
- 在 HbuilderX版本，大于等于4.23时，开放搜索到的设备信息中的serviceData字段
- 更新readme的已知问题描述文案
## 1.0.8（2024-07-17）
- 修复changelog的显示问题
## 1.0.7（2024-07-17）
- 在调用api时发生BLE断联会走失败接口上报 【错误码 10006 】的错误消息 
## 1.0.6（2024-07-17）
- 屏蔽掉某些多余的日志打印
## 1.0.5（2024-06-08）
- 解决了一定要调用一次 `openBluetoothAdapter` 才能注册回调的问题
- 添加了 `batchWriteBLECharacteristicValue` 接口的相关文档
- readme.md文档优化
## 1.0.4（2024-06-07）
- `BLE.js` 添加了 `jsdoc` 进行接口注释，实现了代码提示
- 删掉遗漏的 `getPermissionRequired` 接口相关的定义
- 文档更新，已确认本插件可完美兼容iOS与Android双端的UniAppX引擎工程
## 1.0.3（2024-06-05）
- 移除服务发现的回调里自动请求设置MTU的逻辑，可能会有问题
- 在调用 setBLEMTU 时，检测系统版本是否符合要求，不符合就抛出异常上报给插件使用者
## 1.0.2（2024-06-04）
- 解决了Android11下 `XXPermission` 权限库提示删除 `android:maxSdkVersion` 属性的问题
- 移除了 `getPermissionRequired` 接口，再三思虑，这个接口实在没啥用
- 解决了部分低版本的系统的机型下MTU相关的问题
- 在 `BLE.js` 删除掉错误消息中的 `suppressedExceptions` 字段
- 文档更新，标注一些新发现的问题
## 1.0.1（2024-06-01）
- 修复了IOS端部分接口不走complete回调的bug
- 添加UniAppJS引擎的工程UTSCallback泄露的临时方案补丁
- 文档更新，标注一些新发现的问题
## 1.0.0（2024-05-30）
- (内部开发) 2024-04-29 推翻重构，开始整理接口定义，准备各种来源的资料来确定接口的行为特性进行特性统一
- (内部开发) 2024-05-15 Android端重构完成，接口规范定义，特性对齐统一。开始准备iOS端实现接口
- (内部开发) 2024-05-30 iOS端实现完成，接口规范定义，特性对齐统一，修复大量BUG且测试稳定性通过
- 已确认可发布首版
